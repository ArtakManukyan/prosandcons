import styles from "./coloumn.module.css";
import {Button, TextField, Typography} from "@mui/material";
import {useState} from "react";
import {v4 as uuidv4} from "uuid";

const Columns = () => {
    const [prosText, setProsText] = useState("");
    const [constText, setConsText] = useState("");
    const [pros, setPros] = useState([]);
    const [cons, setCons] = useState([]);
    const [repeat, setRepeat] = useState(false);

    const onEnter = (e, col, edit = null, row, uuid) => {
        setRepeat(false);

        if (e.key === "Enter" && e.target.value && !edit) {
            const newIndex = row.findIndex((index) => index.value === e.target.value);
            if (newIndex === -1) {
                col((prevState) => {
                    const newArr = [...prevState];
                    newArr.push({value: e.target.value, uuid: uuidv4()});
                    return newArr;
                });

                if (col === setPros) {
                    setProsText("");
                } else {
                    setConsText("");
                }
            } else {
                setRepeat(true);
            }
        }

        if (e.key === "Enter" && edit && e.target.value === "") {
            col((prevState) => {
                const newState = [...prevState];
                const deletedItem = newState.findIndex((index) => index.uuid === uuid);
                const newMembers = [
                    ...newState.slice(0, deletedItem),
                    ...newState.slice(deletedItem + 1),
                ];
                return newMembers;
            });
        }
    };

    const editFunc = (row, newValue, setFunc) => {
        const newEdit = {...row};
        if (newEdit.value) {
            newEdit.value = newValue;
            setFunc((prev) => {
                const newState = [...prev];
                let edited = newState.findIndex((index) => index.uuid === newEdit.uuid);
                newState[edited] = newEdit;
                return newState;
            });
        }
    };

    const nextRows = (rows, setFunc) => {
        return rows.map((text) => {
            return (
                <div key={text.uuid} className={styles.rows}>
                    <TextField
                        onKeyPress={(e) => onEnter(e, setFunc, "edit", rows, text.uuid)}
                        value={text.value}
                        onChange={(e) => editFunc(text, e.target.value, setFunc)}
                    />
                </div>
            );
        });
    };

    return (
        <>
            <div>
                <div className={styles.table}>
                    <div className={styles.pros}>
                        <Typography className={styles.title} variant="h4">
                            Pros
                        </Typography>
                        {nextRows(pros, setPros)}
                        <div className={styles.rows}>
                            <TextField
                                onKeyPress={(e) => onEnter(e, setPros, false, cons)}
                                value={prosText}
                                onChange={(e) => setProsText(e.target.value)}
                                InputProps={{
                                    style: {color: repeat ? "red" : "black"},
                                }}
                            />
                        </div>
                    </div>

                    <div className={styles.cons}>
                        <Typography className={styles.title} variant="h4">
                            Cons
                        </Typography>
                        {nextRows(cons, setCons)}
                        <div className={styles.rows}>
                            <TextField
                                onKeyPress={(e) => onEnter(e, setCons, false, pros)}
                                value={constText}
                                onChange={(e) => setConsText(e.target.value)}
                                InputProps={{
                                    style: {color: repeat ? "red" : "black"},
                                }}
                            />
                        </div>
                    </div>
                </div>

                <div className={styles.footer}>
                    <Button
                        onClick={() => {
                            console.log("pros", pros);
                            console.log("cons", cons);
                        }}
                        className={styles.button}
                    >
                        Print
                    </Button>
                </div>
            </div>
        </>
    );
};

export default Columns;
